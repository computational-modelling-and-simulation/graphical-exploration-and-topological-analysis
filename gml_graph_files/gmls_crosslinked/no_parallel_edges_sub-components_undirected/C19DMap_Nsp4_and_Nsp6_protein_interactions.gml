# generated with VANTED V2.8.2 at Fri Mar 04 09:59:54 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 52
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Nsp4 and Nsp6 protein interactions; C19DMap:E protein interactions; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-70106; urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-156540; urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-5228597; urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-9683057; urn:miriam:obo.chebi:CHEBI%3A29235; urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "H_plus_"
      name "H_plus_"
      node_subtype "SIMPLE_MOLECULE; ION"
      node_type "species"
      org_id "layout_49; layout_275; layout_2028; layout_391; layout_254; layout_442; layout_318; layout_68; layout_2211; layout_2369; layout_3569; layout_3547; layout_3602; layout_2226; layout_2388; layout_2348; layout_2421; layout_3554; layout_2934; layout_3624; sa371; sa335; sa370; sa18; sa715; sa20; sa377; sa649; sa644; sa373; sa19; sa234; sa233; sa26; sa57; sa67; sa157; sa137; sa212; sa104; sa235; sa252; sa219; sa25; sa251; sa319; sa98; sa287; sa43; sa190; sa381; sa195"
      uniprot "NA"
    ]
    graphics [
      x 684.9205789728197
      y 1559.0571161764628
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "H_plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Electron Transport Chain disruption; C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:ncbiprotein:AIA62288; urn:miriam:taxonomy:2697049"
      hgnc "NA"
      map_id "Orf9c"
      name "Orf9c"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa498; sa192"
      uniprot "NA"
    ]
    graphics [
      x 829.1981045835628
      y 1253.2240891173965
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf9c"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Electron Transport Chain disruption; C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:hgnc:14247;urn:miriam:refseq:NM_006476;urn:miriam:hgnc.symbol:ATP5MG;urn:miriam:hgnc.symbol:ATP5MG;urn:miriam:ensembl:ENSG00000167283;urn:miriam:ncbigene:10632;urn:miriam:ncbigene:10632;urn:miriam:uniprot:O75964;urn:miriam:uniprot:O75964; urn:miriam:uniprot:O75964;urn:miriam:hgnc:14247;urn:miriam:refseq:NM_006476;urn:miriam:hgnc.symbol:ATP5MG;urn:miriam:hgnc.symbol:ATP5MG;urn:miriam:ensembl:ENSG00000167283;urn:miriam:ncbigene:10632;urn:miriam:ncbigene:10632;urn:miriam:uniprot:O75964; urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049;urn:miriam:hgnc:14247;urn:miriam:refseq:NM_006476;urn:miriam:hgnc.symbol:ATP5MG;urn:miriam:hgnc.symbol:ATP5MG;urn:miriam:ensembl:ENSG00000167283;urn:miriam:ncbigene:10632;urn:miriam:ncbigene:10632;urn:miriam:uniprot:O75964; urn:miriam:hgnc:14247;urn:miriam:refseq:NM_006476;urn:miriam:hgnc.symbol:ATP5MG;urn:miriam:hgnc.symbol:ATP5MG;urn:miriam:ensembl:ENSG00000167283;urn:miriam:ncbigene:10632;urn:miriam:ncbigene:10632;urn:miriam:uniprot:O75964; urn:miriam:uniprot:O75964"
      hgnc "HGNC_SYMBOL:ATP5MG; NA"
      map_id "UNIPROT:O75964"
      name "ATP5MG; F_minus_ATPase; F_minus_ATPase:Nsp6"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa710; csa42; csa43; sa255; csa97"
      uniprot "UNIPROT:O75964"
    ]
    graphics [
      x 371.5355875511551
      y 965.5266500395142
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O75964"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 6
      diagram "C19DMap:Electron Transport Chain disruption; C19DMap:Nsp4 and Nsp6 protein interactions; C19DMap:Interferon 1 pathway; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009742613; urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049; urn:miriam:pubmed:32979938;urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:YP_009742613.1; urn:miriam:ncbiprotein:YP_009725302"
      hgnc "NA"
      map_id "Nsp6"
      name "Nsp6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa713; sa48; sa307; sa304; sa2204; sa2367"
      uniprot "NA"
    ]
    graphics [
      x 779.7082420919924
      y 1103.9505944774487
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Electron Transport Chain disruption; C19DMap:Nsp4 and Nsp6 protein interactions; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009742611; urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761; urn:miriam:ncbiprotein:YP_009725300"
      hgnc "NA"
      map_id "Nsp4"
      name "Nsp4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa695; sa4; sa217; sa2228"
      uniprot "NA"
    ]
    graphics [
      x 688.5339529565322
      y 506.4382940870423
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000133657;urn:miriam:uniprot:Q9H7F0;urn:miriam:hgnc.symbol:ATP13A3;urn:miriam:hgnc.symbol:ATP13A3;urn:miriam:hgnc:24113;urn:miriam:refseq:NM_024524;urn:miriam:ec-code:7.2.2.-;urn:miriam:ncbigene:79572;urn:miriam:ncbigene:79572; urn:miriam:ensembl:ENSG00000133657;urn:miriam:uniprot:Q9H7F0;urn:miriam:hgnc.symbol:ATP13A3;urn:miriam:hgnc.symbol:ATP13A3;urn:miriam:ec-code:7.6.2.16;urn:miriam:hgnc:24113;urn:miriam:refseq:NM_024524;urn:miriam:ncbigene:79572;urn:miriam:ncbigene:79572; urn:miriam:ensembl:ENSG00000133657;urn:miriam:uniprot:Q9H7F0;urn:miriam:hgnc.symbol:ATP13A3;urn:miriam:hgnc.symbol:ATP13A3;urn:miriam:ec-code:7.6.2.16;urn:miriam:hgnc:24113;urn:miriam:refseq:NM_024524;urn:miriam:ncbigene:79572;urn:miriam:ncbigene:79572;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049"
      hgnc "HGNC_SYMBOL:ATP13A3"
      map_id "UNIPROT:Q9H7F0"
      name "ATP13A3; P_minus_ATPase; P_minus_ATPase:Nsp6"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa222; csa41; csa40"
      uniprot "UNIPROT:Q9H7F0"
    ]
    graphics [
      x 1190.7035090357424
      y 1310.726141115592
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9H7F0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_78"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re66"
      uniprot "NA"
    ]
    graphics [
      x 1331.5889992995967
      y 1425.2424370453768
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "P_minus_ATPase"
      name "P_minus_ATPase"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa80"
      uniprot "NA"
    ]
    graphics [
      x 1434.1898554634458
      y 1543.9279701759272
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "P_minus_ATPase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 9
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:10406945;urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1074/jbc.272.43.27107;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:uniprot:Q99720; urn:miriam:pubmed:32353859;urn:miriam:pubmed:10406945;urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1074/jbc.272.43.27107;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:uniprot:Q99720;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049; urn:miriam:pubmed:10406945;urn:miriam:pubmed:10406945;urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1074/jbc.272.43.27107;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:uniprot:Q99720;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049; urn:miriam:pubmed:32353859;urn:miriam:pubmed:10406945;urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1074/jbc.272.43.27107;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:uniprot:Q99720; urn:miriam:pubmed:10406945;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049;urn:miriam:pubmed:10406945;urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1074/jbc.272.43.27107;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:uniprot:Q99720; urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:pubmed:17981125;urn:miriam:taxonomy:10029;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:pubmed:25704011;urn:miriam:uniprot:Q99720;urn:miriam:uniprot:Q99720; urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:uniprot:Q99720;urn:miriam:uniprot:Q99720"
      hgnc "HGNC_SYMBOL:SIGMAR1"
      map_id "UNIPROT:Q99720"
      name "SIGMAR1; Nsp6:SIGMAR1; SIGMAR1:Drugs"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa53; sa226; sa227; csa18; csa19; csa53; csa92; path_1_sa86; path_1_sa82"
      uniprot "UNIPROT:Q99720"
    ]
    graphics [
      x 1123.2252593255464
      y 812.9467744116831
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q99720"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "PUBMED:10406945"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_83"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re73"
      uniprot "NA"
    ]
    graphics [
      x 1024.0085508577854
      y 733.822873375054
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "PUBMED:10406945"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_84"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re74"
      uniprot "NA"
    ]
    graphics [
      x 1094.0719652302355
      y 694.9060787132572
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 6
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:uniprot:Q15904;urn:miriam:ncbigene:537;urn:miriam:ensembl:ENSG00000071553;urn:miriam:ncbigene:537;urn:miriam:hgnc:868;urn:miriam:refseq:NM_001183;urn:miriam:uniprot:Q15904;urn:miriam:pubmed:27231034;urn:miriam:hgnc.symbol:ATP6AP1;urn:miriam:hgnc.symbol:ATP6AP1; urn:miriam:ncbigene:537;urn:miriam:ensembl:ENSG00000071553;urn:miriam:ncbigene:537;urn:miriam:hgnc:868;urn:miriam:refseq:NM_001183;urn:miriam:uniprot:Q15904;urn:miriam:pubmed:27231034;urn:miriam:hgnc.symbol:ATP6AP1;urn:miriam:hgnc.symbol:ATP6AP1; urn:miriam:uniprot:Q15904;urn:miriam:ncbigene:537;urn:miriam:ensembl:ENSG00000071553;urn:miriam:ncbigene:537;urn:miriam:hgnc:868;urn:miriam:refseq:NM_001183;urn:miriam:uniprot:Q15904;urn:miriam:pubmed:27231034;urn:miriam:hgnc.symbol:ATP6AP1;urn:miriam:hgnc.symbol:ATP6AP1;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049; urn:miriam:uniprot:Q15904;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049;urn:miriam:ncbigene:537;urn:miriam:ensembl:ENSG00000071553;urn:miriam:ncbigene:537;urn:miriam:hgnc:868;urn:miriam:refseq:NM_001183;urn:miriam:uniprot:Q15904;urn:miriam:pubmed:27231034;urn:miriam:hgnc.symbol:ATP6AP1;urn:miriam:hgnc.symbol:ATP6AP1"
      hgnc "HGNC_SYMBOL:ATP6AP1"
      map_id "UNIPROT:Q15904"
      name "V_minus_ATPase; ATP6AP1; ATP6AP1:Bafilomycin_space_A1; V_minus_ATPase:Nsp6"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "csa30; csa33; sa223; csa50; csa32; csa34"
      uniprot "UNIPROT:Q15904"
    ]
    graphics [
      x 392.71782526451506
      y 1451.2432961664238
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15904"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_90"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re80"
      uniprot "NA"
    ]
    graphics [
      x 287.9945710405199
      y 1385.3341698347053
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "PUBMED:10406945"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_53"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re28"
      uniprot "NA"
    ]
    graphics [
      x 1029.5066927679152
      y 852.7250330883435
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:obo.go:GO%3A0046611"
      hgnc "NA"
      map_id "V_minus_type_space_proton_space_ATPase"
      name "V_minus_type_space_proton_space_ATPase"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa94"
      uniprot "NA"
    ]
    graphics [
      x 523.9421937556134
      y 1623.3428897890315
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "V_minus_type_space_proton_space_ATPase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_89"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re79"
      uniprot "NA"
    ]
    graphics [
      x 405.36875923533137
      y 1586.7245003836038
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_94"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re9"
      uniprot "NA"
    ]
    graphics [
      x 514.3244509146602
      y 494.02489525492877
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:NUP210;urn:miriam:hgnc.symbol:NUP210;urn:miriam:pubmed:14517331;urn:miriam:uniprot:Q8TEM1;urn:miriam:ncbigene:23225;urn:miriam:ensembl:ENSG00000132182;urn:miriam:ncbigene:23225;urn:miriam:hgnc:30052;urn:miriam:refseq:NM_024923; urn:miriam:pubmed:14517331;urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761;urn:miriam:hgnc.symbol:NUP210;urn:miriam:hgnc.symbol:NUP210;urn:miriam:pubmed:14517331;urn:miriam:uniprot:Q8TEM1;urn:miriam:ncbigene:23225;urn:miriam:ensembl:ENSG00000132182;urn:miriam:ncbigene:23225;urn:miriam:hgnc:30052;urn:miriam:refseq:NM_024923; urn:miriam:pubmed:32353859;urn:miriam:hgnc.symbol:NUP210;urn:miriam:hgnc.symbol:NUP210;urn:miriam:pubmed:14517331;urn:miriam:uniprot:Q8TEM1;urn:miriam:ncbigene:23225;urn:miriam:ensembl:ENSG00000132182;urn:miriam:ncbigene:23225;urn:miriam:hgnc:30052;urn:miriam:refseq:NM_024923;urn:miriam:pubmed:32353859"
      hgnc "HGNC_SYMBOL:NUP210"
      map_id "UNIPROT:Q8TEM1"
      name "NUP210; Nsp4:NUP210; NUP210:Selinexor"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa9; csa14; csa13; csa44; sa224"
      uniprot "UNIPROT:Q8TEM1"
    ]
    graphics [
      x 329.08592656669526
      y 466.12346621971756
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8TEM1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_48"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re23"
      uniprot "NA"
    ]
    graphics [
      x 725.8029959473902
      y 987.8687749952614
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_130"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa49"
      uniprot "NA"
    ]
    graphics [
      x 649.2616300372182
      y 901.9440397425594
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_130"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859"
      hgnc "NA"
      map_id "Nsp3_space_(_minus_)"
      name "Nsp3_space_(_minus_)"
      node_subtype "RNA"
      node_type "species"
      org_id "sa219"
      uniprot "NA"
    ]
    graphics [
      x 515.0775232216015
      y 1417.7958747516057
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp3_space_(_minus_)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_64"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re48"
      uniprot "NA"
    ]
    graphics [
      x 650.0655256343456
      y 1459.073298006925
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_98"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa159"
      uniprot "NA"
    ]
    graphics [
      x 777.9950537811457
      y 1471.7148089057168
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859"
      hgnc "NA"
      map_id "Nsp4_space_(_plus_)"
      name "Nsp4_space_(_plus_)"
      node_subtype "RNA"
      node_type "species"
      org_id "sa3"
      uniprot "NA"
    ]
    graphics [
      x 1124.0386717956708
      y 338.8243573174093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp4_space_(_plus_)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_43"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re19"
      uniprot "NA"
    ]
    graphics [
      x 1271.0828759403607
      y 322.191865363754
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859"
      hgnc "NA"
      map_id "Nsp4_space_(_minus_)"
      name "Nsp4_space_(_minus_)"
      node_subtype "RNA"
      node_type "species"
      org_id "sa220"
      uniprot "NA"
    ]
    graphics [
      x 1391.5677152234782
      y 269.1600015378244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp4_space_(_minus_)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_73"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re6"
      uniprot "NA"
    ]
    graphics [
      x 435.45342863771873
      y 520.3855442489997
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 6
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:uniprot:P14735;urn:miriam:ncbigene:3416;urn:miriam:ncbigene:3416;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1210/mend-4-8-1125;urn:miriam:hgnc:5381;urn:miriam:refseq:NM_004969;urn:miriam:ec-code:3.4.24.56;urn:miriam:ensembl:ENSG00000119912;urn:miriam:hgnc.symbol:IDE;urn:miriam:hgnc.symbol:IDE; urn:miriam:taxonomy:10116;urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761;urn:miriam:uniprot:P14735;urn:miriam:ncbigene:3416;urn:miriam:ncbigene:3416;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1210/mend-4-8-1125;urn:miriam:taxonomy:10116;urn:miriam:hgnc:5381;urn:miriam:refseq:NM_004969;urn:miriam:ec-code:3.4.24.56;urn:miriam:ensembl:ENSG00000119912;urn:miriam:hgnc.symbol:IDE;urn:miriam:hgnc.symbol:IDE; urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761;urn:miriam:uniprot:P14735;urn:miriam:ncbigene:3416;urn:miriam:ncbigene:3416;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1210/mend-4-8-1125;urn:miriam:taxonomy:10116;urn:miriam:hgnc:5381;urn:miriam:refseq:NM_004969;urn:miriam:ec-code:3.4.24.56;urn:miriam:ensembl:ENSG00000119912;urn:miriam:hgnc.symbol:IDE;urn:miriam:hgnc.symbol:IDE; urn:miriam:uniprot:P14735;urn:miriam:ncbigene:3416;urn:miriam:ncbigene:3416;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1210/mend-4-8-1125;urn:miriam:taxonomy:10116;urn:miriam:hgnc:5381;urn:miriam:refseq:NM_004969;urn:miriam:ec-code:3.4.24.56;urn:miriam:ensembl:ENSG00000119912;urn:miriam:hgnc.symbol:IDE;urn:miriam:hgnc.symbol:IDE"
      hgnc "HGNC_SYMBOL:IDE"
      map_id "UNIPROT:P14735"
      name "IDE; Nsp4:IDE; Nsp4_underscore_IDE"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa8; csa2; csa5; sa154; csa4; sa225"
      uniprot "UNIPROT:P14735"
    ]
    graphics [
      x 194.29490410177112
      y 531.3843869803777
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P14735"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_44"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re2"
      uniprot "NA"
    ]
    graphics [
      x 917.2161759730792
      y 405.69511897755956
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "PUBMED:22335796"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_88"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re78"
      uniprot "NA"
    ]
    graphics [
      x 649.3405933026727
      y 1647.094721949371
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_74"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re60"
      uniprot "NA"
    ]
    graphics [
      x 271.29519561391805
      y 1565.4102344795365
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "Bafilomycin_space_A1"
      name "Bafilomycin_space_A1"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa188"
      uniprot "NA"
    ]
    graphics [
      x 163.85175651122
      y 1622.3594981254687
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Bafilomycin_space_A1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_76"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re63"
      uniprot "NA"
    ]
    graphics [
      x 671.0523889076441
      y 616.3081125537101
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:uniprot:Q8N4H5;urn:miriam:uniprot:Q8N4H5;urn:miriam:hgnc:31369;urn:miriam:refseq:NM_001001790;urn:miriam:ensembl:ENSG00000175768;urn:miriam:hgnc.symbol:TOMM5;urn:miriam:hgnc.symbol:TOMM5;urn:miriam:ncbigene:401505;urn:miriam:ncbigene:401505;urn:miriam:ncbigene:10452;urn:miriam:ensembl:ENSG00000130204;urn:miriam:ncbigene:10452;urn:miriam:refseq:NM_001128916;urn:miriam:uniprot:O96008;urn:miriam:uniprot:O96008;urn:miriam:hgnc.symbol:TOMM40;urn:miriam:hgnc.symbol:TOMM40;urn:miriam:hgnc:18001;urn:miriam:ensembl:ENSG00000173726;urn:miriam:refseq:NM_014765;urn:miriam:uniprot:Q15388;urn:miriam:uniprot:Q15388;urn:miriam:ncbigene:9804;urn:miriam:ncbigene:9804;urn:miriam:hgnc.symbol:TOMM20;urn:miriam:hgnc.symbol:TOMM20;urn:miriam:hgnc:20947;urn:miriam:refseq:NM_020243;urn:miriam:uniprot:Q9NS69;urn:miriam:uniprot:Q9NS69;urn:miriam:ncbigene:56993;urn:miriam:ncbigene:56993;urn:miriam:hgnc:18002;urn:miriam:ensembl:ENSG00000100216;urn:miriam:hgnc.symbol:TOMM22;urn:miriam:hgnc.symbol:TOMM22;urn:miriam:refseq:NM_014820;urn:miriam:uniprot:O94826;urn:miriam:uniprot:O94826;urn:miriam:hgnc:11985;urn:miriam:hgnc.symbol:TOMM70;urn:miriam:hgnc.symbol:TOMM70;urn:miriam:ncbigene:9868;urn:miriam:ncbigene:9868;urn:miriam:ensembl:ENSG00000154174;urn:miriam:hgnc:21648;urn:miriam:hgnc.symbol:TOMM7;urn:miriam:hgnc.symbol:TOMM7;urn:miriam:refseq:NM_019059;urn:miriam:ncbigene:54543;urn:miriam:ncbigene:54543;urn:miriam:uniprot:Q9P0U1;urn:miriam:uniprot:Q9P0U1;urn:miriam:ensembl:ENSG00000196683"
      hgnc "HGNC_SYMBOL:TOMM5;HGNC_SYMBOL:TOMM40;HGNC_SYMBOL:TOMM20;HGNC_SYMBOL:TOMM22;HGNC_SYMBOL:TOMM70;HGNC_SYMBOL:TOMM7"
      map_id "UNIPROT:Q8N4H5;UNIPROT:O96008;UNIPROT:Q15388;UNIPROT:Q9NS69;UNIPROT:O94826;UNIPROT:Q9P0U1"
      name "TOM_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa24"
      uniprot "UNIPROT:Q8N4H5;UNIPROT:O96008;UNIPROT:Q15388;UNIPROT:Q9NS69;UNIPROT:O94826;UNIPROT:Q9P0U1"
    ]
    graphics [
      x 618.1949738644398
      y 710.4852604233872
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8N4H5;UNIPROT:O96008;UNIPROT:Q15388;UNIPROT:Q9NS69;UNIPROT:O94826;UNIPROT:Q9P0U1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "PUBMED:14517331"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_41"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re17"
      uniprot "NA"
    ]
    graphics [
      x 358.49550331576035
      y 578.3305641463705
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_59"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re4"
      uniprot "NA"
    ]
    graphics [
      x 1165.894829219263
      y 485.45428239390685
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_135"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa6"
      uniprot "NA"
    ]
    graphics [
      x 1160.8464821183775
      y 649.258143772329
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859"
      hgnc "NA"
      map_id "Nsp3_space_(_plus_)"
      name "Nsp3_space_(_plus_)"
      node_subtype "RNA"
      node_type "species"
      org_id "sa160"
      uniprot "NA"
    ]
    graphics [
      x 525.0613974698072
      y 1182.5813505013514
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp3_space_(_plus_)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_65"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re49"
      uniprot "NA"
    ]
    graphics [
      x 392.50805500989895
      y 1167.484168233737
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_101"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa161"
      uniprot "NA"
    ]
    graphics [
      x 414.4920746593236
      y 1059.7791467788293
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "Several_space_drugs"
      name "Several_space_drugs"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa212"
      uniprot "NA"
    ]
    graphics [
      x 1398.5573486826834
      y 805.2847068155274
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Several_space_drugs"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_75"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re62"
      uniprot "NA"
    ]
    graphics [
      x 1278.0188507282228
      y 820.145366663729
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859"
      hgnc "NA"
      map_id "Nsp6_space_(_minus_)"
      name "Nsp6_space_(_minus_)"
      node_subtype "RNA"
      node_type "species"
      org_id "sa218"
      uniprot "NA"
    ]
    graphics [
      x 794.8436036701578
      y 1904.1707939305206
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp6_space_(_minus_)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_46"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re21"
      uniprot "NA"
    ]
    graphics [
      x 725.5503683258519
      y 1807.276033759448
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859"
      hgnc "NA"
      map_id "Nsp6_space_(_plus_)"
      name "Nsp6_space_(_plus_)"
      node_subtype "RNA"
      node_type "species"
      org_id "sa47"
      uniprot "NA"
    ]
    graphics [
      x 626.7896082872528
      y 1706.6028286993862
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp6_space_(_plus_)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_39"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 583.4347969357846
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "P1"
      name "P1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa84"
      uniprot "NA"
    ]
    graphics [
      x 1382.0923182255015
      y 1775.8614888577576
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "P1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_79"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re67"
      uniprot "NA"
    ]
    graphics [
      x 1473.487623434566
      y 1682.1538684747484
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "P0"
      name "P0"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa83"
      uniprot "NA"
    ]
    graphics [
      x 1354.1753805238584
      y 1688.8590672523678
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "P0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re20"
      uniprot "NA"
    ]
    graphics [
      x 1497.8850205159156
      y 366.29037890573
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_99"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa16"
      uniprot "NA"
    ]
    graphics [
      x 1519.5224115442843
      y 499.6395322056169
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_50"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re25"
      uniprot "NA"
    ]
    graphics [
      x 569.0489551693222
      y 1026.7416551707197
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_69"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re54"
      uniprot "NA"
    ]
    graphics [
      x 890.473286683831
      y 1184.5483282400405
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:uniprot:Q8IY34;urn:miriam:pubmed:32353859;urn:miriam:ncbigene:55117;urn:miriam:ncbigene:55117;urn:miriam:hgnc.symbol:SLC15A3;urn:miriam:hgnc:13621;urn:miriam:refseq:NM_182767;urn:miriam:uniprot:Q9H2J7;urn:miriam:ncbigene:51296;urn:miriam:uniprot:Q9H2J7;urn:miriam:hgnc.symbol:SLC6A15;urn:miriam:hgnc.symbol:SLC6A15;urn:miriam:refseq:NM_018057;urn:miriam:ensembl:ENSG00000072041; urn:miriam:pubmed:32353859;urn:miriam:uniprot:Q8IY34;urn:miriam:pubmed:32353859;urn:miriam:ncbigene:55117;urn:miriam:ncbigene:55117;urn:miriam:hgnc.symbol:SLC15A3;urn:miriam:hgnc:13621;urn:miriam:refseq:NM_182767;urn:miriam:uniprot:Q9H2J7;urn:miriam:ncbigene:51296;urn:miriam:uniprot:Q9H2J7;urn:miriam:hgnc.symbol:SLC6A15;urn:miriam:hgnc.symbol:SLC6A15;urn:miriam:refseq:NM_018057;urn:miriam:ensembl:ENSG00000072041;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049; urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049;urn:miriam:uniprot:Q8IY34;urn:miriam:pubmed:32353859;urn:miriam:ncbigene:55117;urn:miriam:ncbigene:55117;urn:miriam:hgnc.symbol:SLC15A3;urn:miriam:hgnc:13621;urn:miriam:refseq:NM_182767;urn:miriam:uniprot:Q9H2J7;urn:miriam:ncbigene:51296;urn:miriam:uniprot:Q9H2J7;urn:miriam:hgnc.symbol:SLC6A15;urn:miriam:hgnc.symbol:SLC6A15;urn:miriam:refseq:NM_018057;urn:miriam:ensembl:ENSG00000072041; urn:miriam:pubmed:123134323;urn:miriam:uniprot:Q8IY34;urn:miriam:pubmed:32353859;urn:miriam:ncbigene:55117;urn:miriam:ncbigene:55117;urn:miriam:hgnc.symbol:SLC15A3;urn:miriam:hgnc:13621;urn:miriam:refseq:NM_182767;urn:miriam:uniprot:Q9H2J7;urn:miriam:ncbigene:51296;urn:miriam:uniprot:Q9H2J7;urn:miriam:hgnc.symbol:SLC6A15;urn:miriam:hgnc.symbol:SLC6A15;urn:miriam:refseq:NM_018057;urn:miriam:ensembl:ENSG00000072041"
      hgnc "HGNC_SYMBOL:SLC15A3;HGNC_SYMBOL:SLC6A15"
      map_id "UNIPROT:Q8IY34;UNIPROT:Q9H2J7"
      name "SLC6A15; SLC6A15:Nsp6; SLC6A15:Orf9c; SLC6A15:Loratadine"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa191; csa45; csa47; csa49"
      uniprot "UNIPROT:Q8IY34;UNIPROT:Q9H2J7"
    ]
    graphics [
      x 1012.7476842438002
      y 1272.7900224755374
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8IY34;UNIPROT:Q9H2J7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_58"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re37"
      uniprot "NA"
    ]
    graphics [
      x 590.119313860051
      y 329.1090710511354
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000142444;urn:miriam:hgnc:25152;urn:miriam:hgnc.symbol:TIMM29;urn:miriam:hgnc.symbol:TIMM29;urn:miriam:refseq:NM_138358;urn:miriam:uniprot:Q9BSF4;urn:miriam:uniprot:Q9BSF4;urn:miriam:ncbigene:90580;urn:miriam:ncbigene:90580;urn:miriam:hgnc:4022;urn:miriam:uniprot:Q9Y5J6;urn:miriam:uniprot:Q9Y5J6;urn:miriam:hgnc.symbol:TIMM10B;urn:miriam:hgnc.symbol:TIMM10B;urn:miriam:ensembl:ENSG00000132286;urn:miriam:ncbigene:26515;urn:miriam:ncbigene:26515;urn:miriam:refseq:NM_012192;urn:miriam:uniprot:Q9Y584;urn:miriam:uniprot:Q9Y584;urn:miriam:hgnc.symbol:TIMM22;urn:miriam:hgnc.symbol:TIMM22;urn:miriam:hgnc:17317;urn:miriam:ncbigene:29928;urn:miriam:ncbigene:29928;urn:miriam:ensembl:ENSG00000177370;urn:miriam:refseq:NM_013337;urn:miriam:hgnc:11814;urn:miriam:uniprot:P62072;urn:miriam:uniprot:P62072;urn:miriam:ncbigene:26519;urn:miriam:ncbigene:26519;urn:miriam:ensembl:ENSG00000134809;urn:miriam:hgnc.symbol:TIMM10;urn:miriam:hgnc.symbol:TIMM10;urn:miriam:refseq:NM_012456;urn:miriam:refseq:NM_001304485;urn:miriam:ncbigene:26520;urn:miriam:ncbigene:26520;urn:miriam:hgnc.symbol:TIMM9;urn:miriam:ensembl:ENSG00000100575;urn:miriam:hgnc.symbol:TIMM9;urn:miriam:hgnc:11819;urn:miriam:uniprot:Q9Y5J7;urn:miriam:uniprot:Q9Y5J7"
      hgnc "HGNC_SYMBOL:TIMM29;HGNC_SYMBOL:TIMM10B;HGNC_SYMBOL:TIMM22;HGNC_SYMBOL:TIMM10;HGNC_SYMBOL:TIMM9"
      map_id "UNIPROT:Q9BSF4;UNIPROT:Q9Y5J6;UNIPROT:Q9Y584;UNIPROT:P62072;UNIPROT:Q9Y5J7"
      name "TIM_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa29"
      uniprot "UNIPROT:Q9BSF4;UNIPROT:Q9Y5J6;UNIPROT:Q9Y584;UNIPROT:P62072;UNIPROT:Q9Y5J7"
    ]
    graphics [
      x 493.7075078610239
      y 401.2213060168092
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BSF4;UNIPROT:Q9Y5J6;UNIPROT:Q9Y584;UNIPROT:P62072;UNIPROT:Q9Y5J7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:uniprot:Q9Y584;urn:miriam:uniprot:Q9Y584;urn:miriam:hgnc.symbol:TIMM22;urn:miriam:hgnc.symbol:TIMM22;urn:miriam:hgnc:17317;urn:miriam:ncbigene:29928;urn:miriam:ncbigene:29928;urn:miriam:ensembl:ENSG00000177370;urn:miriam:refseq:NM_013337;urn:miriam:hgnc:4022;urn:miriam:uniprot:Q9Y5J6;urn:miriam:uniprot:Q9Y5J6;urn:miriam:hgnc.symbol:TIMM10B;urn:miriam:hgnc.symbol:TIMM10B;urn:miriam:ensembl:ENSG00000132286;urn:miriam:ncbigene:26515;urn:miriam:ncbigene:26515;urn:miriam:refseq:NM_012192;urn:miriam:ensembl:ENSG00000142444;urn:miriam:hgnc:25152;urn:miriam:hgnc.symbol:TIMM29;urn:miriam:hgnc.symbol:TIMM29;urn:miriam:refseq:NM_138358;urn:miriam:uniprot:Q9BSF4;urn:miriam:uniprot:Q9BSF4;urn:miriam:ncbigene:90580;urn:miriam:ncbigene:90580;urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761;urn:miriam:hgnc:11814;urn:miriam:uniprot:P62072;urn:miriam:uniprot:P62072;urn:miriam:ncbigene:26519;urn:miriam:ncbigene:26519;urn:miriam:ensembl:ENSG00000134809;urn:miriam:hgnc.symbol:TIMM10;urn:miriam:hgnc.symbol:TIMM10;urn:miriam:refseq:NM_012456;urn:miriam:refseq:NM_001304485;urn:miriam:ncbigene:26520;urn:miriam:ncbigene:26520;urn:miriam:hgnc.symbol:TIMM9;urn:miriam:ensembl:ENSG00000100575;urn:miriam:hgnc.symbol:TIMM9;urn:miriam:hgnc:11819;urn:miriam:uniprot:Q9Y5J7;urn:miriam:uniprot:Q9Y5J7"
      hgnc "HGNC_SYMBOL:TIMM22;HGNC_SYMBOL:TIMM10B;HGNC_SYMBOL:TIMM29;HGNC_SYMBOL:TIMM10;HGNC_SYMBOL:TIMM9"
      map_id "UNIPROT:Q9Y584;UNIPROT:Q9Y5J6;UNIPROT:Q9BSF4;UNIPROT:P62072;UNIPROT:Q9Y5J7"
      name "TIM_space_complex:Nsp4"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa27"
      uniprot "UNIPROT:Q9Y584;UNIPROT:Q9Y5J6;UNIPROT:Q9BSF4;UNIPROT:P62072;UNIPROT:Q9Y5J7"
    ]
    graphics [
      x 468.8570133422444
      y 299.8811747634768
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9Y584;UNIPROT:Q9Y5J6;UNIPROT:Q9BSF4;UNIPROT:P62072;UNIPROT:Q9Y5J7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      annotation "PUBMED:10406945"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_85"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re75"
      uniprot "NA"
    ]
    graphics [
      x 1215.5100876670465
      y 723.3647907071802
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:hgnc:32456;urn:miriam:ec-code:2.4.1.131;urn:miriam:ncbigene:440138;urn:miriam:ncbigene:440138;urn:miriam:uniprot:Q2TAA5;urn:miriam:hgnc.symbol:ALG11;urn:miriam:hgnc.symbol:ALG11;urn:miriam:pubmed:20080937;urn:miriam:refseq:NM_001004127;urn:miriam:ensembl:ENSG00000253710; urn:miriam:pubmed:20080937;urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761;urn:miriam:hgnc:32456;urn:miriam:ec-code:2.4.1.131;urn:miriam:ncbigene:440138;urn:miriam:ncbigene:440138;urn:miriam:uniprot:Q2TAA5;urn:miriam:hgnc.symbol:ALG11;urn:miriam:hgnc.symbol:ALG11;urn:miriam:pubmed:20080937;urn:miriam:refseq:NM_001004127;urn:miriam:ensembl:ENSG00000253710"
      hgnc "HGNC_SYMBOL:ALG11"
      map_id "UNIPROT:Q2TAA5"
      name "ALG11; Nsp4_underscore_ALG11"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa14; sa230; csa15; csa93"
      uniprot "UNIPROT:Q2TAA5"
    ]
    graphics [
      x 832.4016356976883
      y 179.88853489402743
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q2TAA5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_87"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re77"
      uniprot "NA"
    ]
    graphics [
      x 843.7681800614693
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_38"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1"
      uniprot "NA"
    ]
    graphics [
      x 1249.8073719091744
      y 238.41354360533228
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "PUBMED:9830016"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_60"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re40"
      uniprot "NA"
    ]
    graphics [
      x 181.02784962375472
      y 654.114810358031
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_71"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re56"
      uniprot "NA"
    ]
    graphics [
      x 1157.9573956209947
      y 1112.449034286587
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 9
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions; C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:taxonomy:2697049;urn:miriam:uniprot:M; urn:miriam:pubmed:31226023;urn:miriam:ncbiprotein:YP_009724393.1;urn:miriam:uniprot:M; urn:miriam:ncbiprotein:1796318601;urn:miriam:uniprot:M; urn:miriam:ncbiprotein:APO40582;urn:miriam:pubmed:16845612;urn:miriam:uniprot:M"
      hgnc "NA"
      map_id "UNIPROT:M"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa193; sa134; sa226; sa232; sa408; sa359; sa353; sa403; sa42"
      uniprot "UNIPROT:M"
    ]
    graphics [
      x 1224.1656227768565
      y 1010.8263573753504
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:M"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:uniprot:Q8IY34;urn:miriam:pubmed:32353859;urn:miriam:ncbigene:55117;urn:miriam:ncbigene:55117;urn:miriam:hgnc.symbol:SLC15A3;urn:miriam:hgnc:13621;urn:miriam:refseq:NM_182767;urn:miriam:uniprot:Q9H2J7;urn:miriam:ncbigene:51296;urn:miriam:uniprot:Q9H2J7;urn:miriam:hgnc.symbol:SLC6A15;urn:miriam:hgnc.symbol:SLC6A15;urn:miriam:refseq:NM_018057;urn:miriam:ensembl:ENSG00000072041;urn:miriam:taxonomy:2697049;urn:miriam:uniprot:M"
      hgnc "HGNC_SYMBOL:SLC15A3;HGNC_SYMBOL:SLC6A15"
      map_id "UNIPROT:Q8IY34;UNIPROT:Q9H2J7;UNIPROT:M"
      name "SLC6A15:M"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa48"
      uniprot "UNIPROT:Q8IY34;UNIPROT:Q9H2J7;UNIPROT:M"
    ]
    graphics [
      x 1285.217485134623
      y 1091.4882681187792
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8IY34;UNIPROT:Q9H2J7;UNIPROT:M"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_55"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re31"
      uniprot "NA"
    ]
    graphics [
      x 597.6212577377012
      y 1279.2930980733254
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_57"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re33"
      uniprot "NA"
    ]
    graphics [
      x 878.8099009448387
      y 1790.2710087079568
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_134"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa58"
      uniprot "NA"
    ]
    graphics [
      x 879.1414658850001
      y 1635.7034017737994
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_63"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re47"
      uniprot "NA"
    ]
    graphics [
      x 701.44915000611
      y 1122.627455602844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 10
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions; C19DMap:Interferon 1 pathway; C19DMap:Virus replication cycle; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049;urn:miriam:uniprot:Nsp3; urn:miriam:pubmed:32353859;urn:miriam:doi:10.1016/j.virol.2017.07.019;urn:miriam:taxonomy:694009;urn:miriam:pubmed:29128390;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049;urn:miriam:uniprot:Nsp3;urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761; urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:ncbiprotein:YP_009725299;urn:miriam:uniprot:Nsp3; urn:miriam:ncbiprotein:YP_009725299;urn:miriam:uniprot:Nsp3; urn:miriam:ncbiprotein:1802476807;urn:miriam:uniprot:Nsp3"
      hgnc "NA"
      map_id "UNIPROT:Nsp3"
      name "Nsp3; Nsp3:Nsp4:Nsp6"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa162; csa39; sa123; sa169; sa2222; sa357; sa361; sa349; sa354; sa363"
      uniprot "UNIPROT:Nsp3"
    ]
    graphics [
      x 906.5382811400968
      y 1053.20130329673
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Nsp3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re22"
      uniprot "NA"
    ]
    graphics [
      x 708.7841885380718
      y 1396.9173315263733
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_80"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re7"
      uniprot "NA"
    ]
    graphics [
      x 105.30365116602036
      y 640.5490292761812
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_40"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re16"
      uniprot "NA"
    ]
    graphics [
      x 804.9291201857446
      y 476.5170867801244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:uniprot:Q9NVH1;urn:miriam:hgnc:25570;urn:miriam:ncbigene:55735;urn:miriam:ncbigene:55735;urn:miriam:pubmed:32353859;urn:miriam:pubmed:25997101;urn:miriam:ensembl:ENSG00000007923;urn:miriam:hgnc.symbol:DNAJC11;urn:miriam:refseq:NM_018198;urn:miriam:hgnc.symbol:DNAJC11; urn:miriam:pubmed:32353859;urn:miriam:pubmed:25997101;urn:miriam:uniprot:Q9NVH1;urn:miriam:hgnc:25570;urn:miriam:ncbigene:55735;urn:miriam:ncbigene:55735;urn:miriam:pubmed:32353859;urn:miriam:pubmed:25997101;urn:miriam:ensembl:ENSG00000007923;urn:miriam:hgnc.symbol:DNAJC11;urn:miriam:refseq:NM_018198;urn:miriam:hgnc.symbol:DNAJC11;urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761"
      hgnc "HGNC_SYMBOL:DNAJC11"
      map_id "UNIPROT:Q9NVH1"
      name "DNAJC11; Nsp4:DNAJC11"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa13; csa12"
      uniprot "UNIPROT:Q9NVH1"
    ]
    graphics [
      x 880.4260536499296
      y 553.0132050442062
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9NVH1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_61"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re45"
      uniprot "NA"
    ]
    graphics [
      x 510.1645957365489
      y 1304.4326171257596
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re3"
      uniprot "NA"
    ]
    graphics [
      x 713.045967710277
      y 387.204595852528
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_131"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa5"
      uniprot "NA"
    ]
    graphics [
      x 613.1003883172381
      y 406.8066552434666
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_131"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_51"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re26"
      uniprot "NA"
    ]
    graphics [
      x 979.8257100094777
      y 1188.6439669864326
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_70"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re55"
      uniprot "NA"
    ]
    graphics [
      x 910.7872600822357
      y 1342.9065263725006
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_86"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re76"
      uniprot "NA"
    ]
    graphics [
      x 732.8564625045552
      y 129.42161671038002
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_92"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re82"
      uniprot "NA"
    ]
    graphics [
      x 274.96800553139076
      y 1033.538781260255
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "F1"
      name "F1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa101"
      uniprot "NA"
    ]
    graphics [
      x 136.02754942811742
      y 876.2877054367945
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "F1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_93"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re83"
      uniprot "NA"
    ]
    graphics [
      x 202.11113967820108
      y 964.7778550292007
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "F0"
      name "F0"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa100"
      uniprot "NA"
    ]
    graphics [
      x 84.87143228074319
      y 1004.3230518804371
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "F0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_91"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re81"
      uniprot "NA"
    ]
    graphics [
      x 271.44475251378407
      y 1472.2926129519094
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_68"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re53"
      uniprot "NA"
    ]
    graphics [
      x 211.81349146798897
      y 385.09789052701626
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859"
      hgnc "NA"
      map_id "Selinexor"
      name "Selinexor"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa187"
      uniprot "NA"
    ]
    graphics [
      x 91.8807439118167
      y 386.41118813818
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Selinexor"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_42"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re18"
      uniprot "NA"
    ]
    graphics [
      x 773.3402750149547
      y 322.46982785138107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_77"
      name "NA"
      node_subtype "POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "re64"
      uniprot "NA"
    ]
    graphics [
      x 1079.6770210420711
      y 1122.4564408923377
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:obo.go:GO%3A0007009;urn:miriam:taxonomy:694009;urn:miriam:pubmed:23943763"
      hgnc "NA"
      map_id "Plasma_space_membrane_space_organization"
      name "Plasma_space_membrane_space_organization"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa164"
      uniprot "NA"
    ]
    graphics [
      x 1205.1332842854165
      y 1174.4393138344585
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Plasma_space_membrane_space_organization"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      annotation "PUBMED:14517331"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_81"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re71"
      uniprot "NA"
    ]
    graphics [
      x 283.92623009621116
      y 598.9126485424101
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_66"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re50"
      uniprot "NA"
    ]
    graphics [
      x 1048.9070646702753
      y 1193.370649351502
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_103"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa163"
      uniprot "NA"
    ]
    graphics [
      x 1108.9154073072318
      y 1308.0306938345973
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_56"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re32"
      uniprot "NA"
    ]
    graphics [
      x 663.6781874202986
      y 1865.6389046677566
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_62"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re46"
      uniprot "NA"
    ]
    graphics [
      x 433.8978368166191
      y 1299.5908532761368
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_82"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re72"
      uniprot "NA"
    ]
    graphics [
      x 68.62937610571521
      y 508.6184274702075
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_52"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re27"
      uniprot "NA"
    ]
    graphics [
      x 951.7289807502302
      y 948.313611827168
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_72"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re57"
      uniprot "NA"
    ]
    graphics [
      x 1044.203492853444
      y 1414.3236765072334
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:123134323"
      hgnc "NA"
      map_id "Loratadine"
      name "Loratadine"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa200"
      uniprot "NA"
    ]
    graphics [
      x 1074.8431238450532
      y 1525.240473079879
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Loratadine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859;PUBMED:29128390"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_67"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re51"
      uniprot "NA"
    ]
    graphics [
      x 787.2681809902764
      y 848.2539624819108
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_49"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re24"
      uniprot "NA"
    ]
    graphics [
      x 513.1921377234927
      y 1803.3281226482575
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_132"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa50"
      uniprot "NA"
    ]
    graphics [
      x 449.0277932596286
      y 1906.27032587155
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 103
    source 6
    target 7
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9H7F0"
      target_id "M14_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 8
    target 7
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "P_minus_ATPase"
      target_id "M14_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 9
    target 10
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q99720"
      target_id "M14_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 106
    source 9
    target 11
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q99720"
      target_id "M14_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 107
    source 12
    target 13
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15904"
      target_id "M14_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 108
    source 9
    target 14
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q99720"
      target_id "M14_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 109
    source 15
    target 16
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "V_minus_type_space_proton_space_ATPase"
      target_id "M14_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 110
    source 12
    target 16
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15904"
      target_id "M14_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 111
    source 5
    target 17
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Nsp4"
      target_id "M14_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 112
    source 18
    target 17
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TEM1"
      target_id "M14_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 113
    source 4
    target 19
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Nsp6"
      target_id "M14_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 114
    source 19
    target 20
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_48"
      target_id "M14_130"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 21
    target 22
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Nsp3_space_(_minus_)"
      target_id "M14_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 22
    target 23
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_64"
      target_id "M14_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 24
    target 25
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Nsp4_space_(_plus_)"
      target_id "M14_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 25
    target 26
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_43"
      target_id "Nsp4_space_(_minus_)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 5
    target 27
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Nsp4"
      target_id "M14_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 28
    target 27
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P14735"
      target_id "M14_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 24
    target 29
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Nsp4_space_(_plus_)"
      target_id "M14_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 29
    target 5
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_44"
      target_id "Nsp4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 1
    target 30
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "H_plus_"
      target_id "M14_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 15
    target 30
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CATALYSIS"
      source_id "V_minus_type_space_proton_space_ATPase"
      target_id "M14_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 12
    target 31
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15904"
      target_id "M14_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 126
    source 32
    target 31
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Bafilomycin_space_A1"
      target_id "M14_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 127
    source 5
    target 33
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Nsp4"
      target_id "M14_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 128
    source 34
    target 33
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q8N4H5;UNIPROT:O96008;UNIPROT:Q15388;UNIPROT:Q9NS69;UNIPROT:O94826;UNIPROT:Q9P0U1"
      target_id "M14_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 18
    target 35
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TEM1"
      target_id "M14_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 130
    source 24
    target 36
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Nsp4_space_(_plus_)"
      target_id "M14_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 131
    source 36
    target 37
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_59"
      target_id "M14_135"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 132
    source 38
    target 39
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Nsp3_space_(_plus_)"
      target_id "M14_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 133
    source 39
    target 40
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_65"
      target_id "M14_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 134
    source 41
    target 42
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Several_space_drugs"
      target_id "M14_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 135
    source 9
    target 42
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q99720"
      target_id "M14_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 136
    source 43
    target 44
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Nsp6_space_(_minus_)"
      target_id "M14_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 137
    source 44
    target 45
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_46"
      target_id "Nsp6_space_(_plus_)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 138
    source 28
    target 46
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P14735"
      target_id "M14_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 139
    source 47
    target 48
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "P1"
      target_id "M14_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 49
    target 48
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "P0"
      target_id "M14_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 48
    target 8
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_79"
      target_id "P_minus_ATPase"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 26
    target 50
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Nsp4_space_(_minus_)"
      target_id "M14_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 50
    target 51
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_45"
      target_id "M14_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 144
    source 4
    target 52
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Nsp6"
      target_id "M14_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 145
    source 3
    target 52
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O75964"
      target_id "M14_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 146
    source 4
    target 53
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Nsp6"
      target_id "M14_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 147
    source 54
    target 53
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8IY34;UNIPROT:Q9H2J7"
      target_id "M14_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 148
    source 5
    target 55
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Nsp4"
      target_id "M14_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 149
    source 56
    target 55
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BSF4;UNIPROT:Q9Y5J6;UNIPROT:Q9Y584;UNIPROT:P62072;UNIPROT:Q9Y5J7"
      target_id "M14_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 150
    source 55
    target 57
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_58"
      target_id "UNIPROT:Q9Y584;UNIPROT:Q9Y5J6;UNIPROT:Q9BSF4;UNIPROT:P62072;UNIPROT:Q9Y5J7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 151
    source 9
    target 58
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q99720"
      target_id "M14_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 152
    source 59
    target 60
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q2TAA5"
      target_id "M14_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 153
    source 26
    target 61
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Nsp4_space_(_minus_)"
      target_id "M14_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 154
    source 61
    target 24
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_38"
      target_id "Nsp4_space_(_plus_)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 155
    source 28
    target 62
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P14735"
      target_id "M14_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 156
    source 54
    target 63
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8IY34;UNIPROT:Q9H2J7"
      target_id "M14_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 157
    source 64
    target 63
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:M"
      target_id "M14_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 158
    source 63
    target 65
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_71"
      target_id "UNIPROT:Q8IY34;UNIPROT:Q9H2J7;UNIPROT:M"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 159
    source 4
    target 66
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Nsp6"
      target_id "M14_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 160
    source 12
    target 66
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15904"
      target_id "M14_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 161
    source 43
    target 67
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Nsp6_space_(_minus_)"
      target_id "M14_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 162
    source 67
    target 68
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_57"
      target_id "M14_134"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 163
    source 38
    target 69
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Nsp3_space_(_plus_)"
      target_id "M14_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 164
    source 69
    target 70
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_63"
      target_id "UNIPROT:Nsp3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 165
    source 45
    target 71
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Nsp6_space_(_plus_)"
      target_id "M14_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 166
    source 71
    target 4
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_47"
      target_id "Nsp6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 167
    source 28
    target 72
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P14735"
      target_id "M14_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 168
    source 5
    target 73
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Nsp4"
      target_id "M14_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 169
    source 74
    target 73
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9NVH1"
      target_id "M14_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 170
    source 21
    target 75
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Nsp3_space_(_minus_)"
      target_id "M14_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 171
    source 75
    target 38
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_61"
      target_id "Nsp3_space_(_plus_)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 172
    source 5
    target 76
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Nsp4"
      target_id "M14_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 173
    source 76
    target 77
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_54"
      target_id "M14_131"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 174
    source 4
    target 78
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Nsp6"
      target_id "M14_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 175
    source 6
    target 78
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9H7F0"
      target_id "M14_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 176
    source 54
    target 79
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8IY34;UNIPROT:Q9H2J7"
      target_id "M14_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 177
    source 2
    target 79
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Orf9c"
      target_id "M14_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 178
    source 59
    target 80
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q2TAA5"
      target_id "M14_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 179
    source 3
    target 81
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O75964"
      target_id "M14_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 180
    source 82
    target 83
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "F1"
      target_id "M14_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 181
    source 84
    target 83
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "F0"
      target_id "M14_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 182
    source 83
    target 3
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_93"
      target_id "UNIPROT:O75964"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 12
    target 85
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15904"
      target_id "M14_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 18
    target 86
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TEM1"
      target_id "M14_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 87
    target 86
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Selinexor"
      target_id "M14_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 5
    target 88
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Nsp4"
      target_id "M14_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 59
    target 88
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q2TAA5"
      target_id "M14_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 70
    target 89
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Nsp3"
      target_id "M14_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 89
    target 90
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_77"
      target_id "Plasma_space_membrane_space_organization"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 18
    target 91
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TEM1"
      target_id "M14_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 70
    target 92
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Nsp3"
      target_id "M14_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 92
    target 93
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_66"
      target_id "M14_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 45
    target 94
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Nsp6_space_(_plus_)"
      target_id "M14_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 94
    target 43
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_56"
      target_id "Nsp6_space_(_minus_)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 38
    target 95
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Nsp3_space_(_plus_)"
      target_id "M14_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 95
    target 21
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_62"
      target_id "Nsp3_space_(_minus_)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 28
    target 96
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P14735"
      target_id "M14_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 4
    target 97
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Nsp6"
      target_id "M14_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 9
    target 97
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q99720"
      target_id "M14_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 54
    target 98
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8IY34;UNIPROT:Q9H2J7"
      target_id "M14_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 99
    target 98
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Loratadine"
      target_id "M14_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 70
    target 100
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Nsp3"
      target_id "M14_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 4
    target 100
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Nsp6"
      target_id "M14_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 5
    target 100
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Nsp4"
      target_id "M14_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 45
    target 101
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Nsp6_space_(_plus_)"
      target_id "M14_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 101
    target 102
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_49"
      target_id "M14_132"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
