# Centralities folder

**The centralities folder hosts the files that contain centrality calculations created with the Vanted tool.**

Files are in tsv format and have the headers

Position	VertexLabel	Eccentricity	Degree	Eigenvector	HITSAuths	CurrentFlowBetweenness	Radiality	Stress	SPBetweenness	Centroid	HubbelIndex0.05	Closeness	KatzStatus0.05	HITSHubs	OutDegree	InDegree	PageRank0.15	CurrentFlowCloseness


Numbers in the headers indicate the parameter settings in Vanted for a centrality.

There are five full network files which are based on different sources:
1. The crosslinked bipartite graph ("the main" network for analysis, all others are just for comparison)
2. a Minerva graph 
3. a WikiPathways graph 
4. Two Reactome graphs


The main network file (1.) that was created from merging all pathways and that is the base for the reported results is the 'COVID19_Disease_Map_bipartite_crosslinked.gml'.
In addition, there are two folders for individual results on pathways from Minerva and Wikipathways.
For all graphs the largest component was taken in case the graph is not connected (the tiny remaining components are not a reasonable source of centralities).
For all entities in the network, we take into account a list of entities to filter out ("filtered.csv", e.g. reactions) and a list of entities to definitely keep ("species.csv").

# Rankings folder

**The rankings folder contains the results of the ranking calculations.**

Each result file in xlsx format contains two result sheets out of technical reasons. The format is used to allow calculations with aggregation across multiple graphs - you have one sheet for the aggregation, called "Top ranking all occurrences", and one for each graph file that was analysed.
As we only use one graph file for each ranking calculation, there are two sheets, and the "across all occurrences" information is nothing else but "for the occurrence in that single graph".

In the top ranking sheet, you find a list of the top ranked entities according to an aggregated importance value based on all its computed centrality values for an entity (description see below). 
In the files there are three aggregated values listed, where two are given only as reference for older calculation comparison, and the ranking criterion is in the third column "aggrankrsscompl" (description see below), next to the label, which is followed by individual centrality rankings for that entry.

For example a snippet of two partial entries for the top ranked UNIPROT:P0DTD1 and UNIPROT:P0DTC9:

Index | aggrank_sum | aggrank | aggrankrsscompl | Vertexlabel | Eccentricity_rank | Degree_rank | Eigenvector_rank | ...
--- | --- | --- | --- | --- | --- | --- | --- | ---
47 | 0.250012082 | 0.203729006 | 3.088928834 | UNIPROT:P0DTD1 | 0.2 | 0.038461538 | 0.000702741 | ...
34 | 0.343985169 | 0.231282496 | 3.060313914 | UNIPROT:P0DTC9 | 0.2 | 0.115384615 | 0.001405481 | ...

Below that follows a list that is supposed to show an aggregation of values in case a node appears multiple times across several graphs (the top ranking would show all occurrences individually), but as we do not have this case, you can ignore it as it gives the same values as the top list. 

In the second sheet you first find some details on the rankings per centrality, the top ten entities for each of them. Coloring is based on the zscore to visually indicate how discriminating / important the ranking is (the darker the color and the bigger the color difference between entries, the more important).
Below that some statistics on centrality values - this is simply some automated python profiling.
After that follow the raw centrality values for all entities. 

The rankings were calculated from the centralities as follows:

- First we calculate the known set of centralities for each graph in Vanted (simply everything the tool has to offer).
- Then, we normalise the values as they differ a lot in scale.
- For the normalised values we calculate percentile rankings. This is done to assure that two centrality vectors (simplified to three centralities for explanation) of (1,0.5,0.2) and  (0.2,0.5,1) don't give the same value in case the distribution of values in the three centralities is different  - e.g. 0.2 might be second highest on the right and second lowest on the left.
- As now small values show high importance, an aggregation across centralities e.g. by RSS could be biased towards large, i.e. unimportant values. Thus, we calculate the complement 1-v for each centrality ranking value.
- For these complements, we calculate RSS (root sum square) as the aggregated "importance" value for each entity and call it aggrankrsscompl in the tables.

# Filters folder

**The filter folders contains files that were used to filter certain species of interest.**
